package ua.nure.Mamontov.Practice2;

import java.util.Arrays;
import java.util.Iterator;

public class MyListImpl implements MyList, ListIterable{
	
	private class IteratorImpl implements Iterator<Object> {
		private int element = -1;
		private int iter = 0;
		
		/**
		 *	returns true if the iteration has more elements
		 */
		@Override
		public boolean hasNext() {
			boolean rezult = false;
			if (array[element + 1] != null) {
				rezult = true;
			}
			return rezult;
		}
		
		/**
		 * 	returns the next element in the iteration
		 */
		@Override
		public Object next() {
			Object obj;	
			if (hasNext()) {
				obj = array[element + 1];
				element++;
				iter++;
			} else {
				obj = null;
			}			
			return obj;
		}
		
		/**
		 * 	removes from the underlying collection the last element
		 *	returned by this iterator
		 */
		@Override
		public void remove() {
			if (iter == 0) {
				throw new IllegalStateException();
			} else {
				MyListImpl.this.remove(array[element]);
				element--;
				iter = 0;
			}
		}
	}
	
	private class ListIteratorImpl extends IteratorImpl implements ListIterator {
		private int element = size();
		/** 
		 * 	returns true if this list iterator
		 *  has more elements when traversing
		 *	the list in the reverse direction
		 */
		@Override
		public boolean hasPrevious() {
			boolean rezult = false;
			if (element > 0) {
				rezult = true;
			}
			return rezult;
		}
		
		/**
		 *	returns the previous element
		 *	in the list and moves the cursor
		 *	position backwards
		 */
		@Override
		public Object previous() {
			Object obj = null;
			if (hasPrevious()) {
				obj = array[element - 1];
				element--;
			}
			
			return obj;
		}

		/** 
		 * 	replaces the last element returned
		 *  by next or previous with
		 * 	the specified element
		 */
		@Override
		public void set(Object e) {
			if (element < size() -1) {
				array[element + 1] = e;
			} else {
				array[element - 1] = e;
			}
		}		
	}

	
	private final static int SIZE_ARRAY = 10;
	private int size = 0;
	private Object[] array = new Object[SIZE_ARRAY];
	
	/** 
	 * 	appends the specified element to the end of this list
	 */
	@Override
	public void add(Object e) {
		if (size == array.length) {
			Object[] temp = array;
			array = new Object[temp.length * 2];
			for (int i = 0; i < temp.length; i++) {
				array[i] = temp[i];
			}
		}
		array[size] = e;
		size++;
	}
	
	/**
	 * 	removes all of the elements from this list
	 */
	@Override
	public void clear() {
		size = 0;
		array = new Object[SIZE_ARRAY];
	
	}
	
	/**
	 * 	removes the first occurrence of the specified element
	 * 	from this list
	 */
	@Override
	public boolean remove(Object o) {
		boolean rezult = false;
		
		for (int i = 0; i < array.length; i++) {
			if (array[i] != null) {
				if (array[i].equals(o)) {
					rezult = true;
					for (int j = i; j < size - 1; j++) {
						array[j] = array[j + 1];
					}
				}
				
			} else {
				continue;
			}
		}
		--size;
		return rezult;
	}
	
	/**
	 *	returns an array containing all of the elements
	 *	in this list in proper sequence
	 */
	@Override
	public Object[] toArray() {
		return Arrays.copyOf(array, size);
	}
	
	/**
	 * 	returns the number of elements in this list
	 */
	@Override
	public int size() {
		return size;
	}
	
	/**
	 * 	returns true if this list contains the specified element
	 */
	@Override
	public boolean contains(Object o) {
		boolean rezult = false;
		for (int i = 0; i < array.length; i++) {
			if (array[i] != null) {
				if (array[i].equals(o)) {
					rezult = true;
				} else {
					rezult = false;
				}
			} else {
				if (o == null) {
					rezult = true;
				}
			}
		}
		return rezult;
	}
	
	/**
	 *	returns true if this list contains all of the elements
	 * 	of the specified list
	 */
	@Override
	public boolean containsAll(MyList c) {
		boolean rezult = false;
		Object[] obj = c.toArray();
		
	i:	for(int i = 0; i < c.size(); i++) {
			for (int j = 0; j < array.length; j++) {
				if(obj[i].equals(array[j])) {
					rezult = true;
					continue i;
				} else if ( j == array.length - 1){
					rezult = false;
					break i;
				}
			}
		}
		return rezult;
	}
	
	//READY
	@Override
	public String toString() {
		String str = "";
		str += "array = [";
		for (int i = 0; i < array.length; i++) {
			
			if (size == 0) {
				str += " ";
				break;
			}
			
			str += array[i];
			
			if( i == (size - 1)) {
				break;
			} 
			str += ", ";
		}
		str += "]";
		return str;
	}

	//READY
	@Override
	public Iterator<Object> iterator() {
		return new IteratorImpl();
	}

	//READY
	@Override
	public ListIterator listIterator() {
		return new ListIteratorImpl();
	}	
	
}